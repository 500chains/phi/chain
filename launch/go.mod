module gitlab.com/500chains/phi/launch

go 1.14

require (
	github.com/alfg/blockchain v0.0.0-20170304071410-09d957063c58
	github.com/cosmos/cosmos-sdk v0.38.2
	github.com/tendermint/go-amino v0.15.1
	github.com/tendermint/tendermint v0.33.2
)

replace github.com/cosmos/gaia => gitlab.com/500chains/phi/chain/app latest
