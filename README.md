|[500chains](https://gitlab.com/500chains)|[phi](https://gitlab.com/500chains/phi/chain)|readme|
|---|---|---|

Cosmos SDK / Tendermint based blockchain showcase and testbed for module development services

# documentation

- [working with testnets](docs/testnet.md)
- modules
  - [~~executor~~](docs/executor.md) ~~staked disbursement scheduler with interest bearing inflation~~
  - [~~notary~~](docs/notary.md) ~~storing data permanently in the blockchain - inflation bearing stake?~~

